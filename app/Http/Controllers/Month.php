<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Month extends BaseController
{
	public function __invoke()
    {
        for ($i = 0; $i < 12; $i++) {
			$time = strtotime(sprintf('%d months', $i));   
			$label = date('F', $time);   
			$value = date('M', $time);
			echo $value;
			echo date('m',strtotime($value));
			echo ":";
			echo $label;									
			echo "<br />";
		}
    }
}
